-module(gleam_file_bridge).

-export([user_home/0, cwd/0, extension/1, write_string/2, os_family/0, delete/1,
         list_directory/1, delete_empty_directory/1, delete_non_empty_directory/1,
         make_directory/1, exists/1, ensure_directory/1, cd/1]).

%% @spec get_user_home() -> {ok, bitstring()} | {error, nil}.
%% @doc Get the current user's home directory. If this returns nil then
%% HOME needs to be set in the environment
user_home() ->
    case init:get_argument(home) of
        {ok, [[Home] | _]} ->
            Encoding = file:native_name_encoding(),
            {ok, unicode:characters_to_binary(Home, Encoding, unicode)};
        _ ->
            {error, nil}
    end.

%% @spec cwd() -> {ok, bitstring()} | {error, atom()}.
%% @doc Get the current working directory
cwd() ->
    case file:get_cwd() of
        {ok, Cwd} ->
            Encoding = file:native_name_encoding(),
            {ok, unicode:characters_to_binary(Cwd, Encoding, unicode)};
        {error, Posix} ->
            {error, Posix}
    end.

%% @spec extension(bitstring()) -> {ok, bitstring()} | {error, nil}.
%% @doc get the extension of a filename including the dot "."
extension(Path) ->
    case filename:extension(Path) of
        [] ->
            {error, nil};
        E ->
            chardata_to_string(E)
    end.

%% @spec write_string(bitstring(), bitstring()) -> {ok, nil} | {error, atom()}.
%% @doc write a string to a file, creating or replacing file where applicable
write_string(Path, Contents) ->
    case file:write_file(Path, Contents) of
        ok ->
            {ok, nil};
        {error, Reason} ->
            {error, Reason}
    end.

%% @spec delete(bitstring()) -> {ok, nil} | {error, atom()}.
%% @doc attempt to delete a file
delete(Path) ->
    case file:delete(Path) of
        ok ->
            {ok, nil};
        {error, Reason} ->
            {error, Reason}
    end.

%% @spec list_directory(bitsting()) -> {ok, [bitstring()} | {error, atom()}
%% @doc list the contents of a directory
list_directory(Path) ->
    case file:list_dir(Path) of
        {error, Reason} ->
            {error, Reason};
        {ok, Names} ->
            Encoding = file:native_name_encoding(),
            UnicodePaths = [unicode:characters_to_binary(N, Encoding, unicode) || N <- Names],
            {ok, UnicodePaths}
    end.

%% @doc return the family of os the node is running on
%% @spec os_family() -> win32 | unix
os_family() ->
    case os:type() of
        {win32, _} ->
            win32;
        {unix, _} ->
            unix
    end.

%% @doc delete an empty directory
%% @spec delete_empty_directory(bitstring()) -> {ok, nil} | {error, atom()}
delete_empty_directory(Path) ->
    case file:del_dir(Path) of
        ok ->
            {ok, nil};
        X ->
            X
    end.

%% @doc delete a non empty directory
%% @spec delete_non_empty_directory(bitstring()) -> {ok, nil} | {error, atom()}
delete_non_empty_directory(Path) ->
    case file:del_dir_r(Path) of
        ok ->
            {ok, nil};
        X ->
            X
    end.

%% @doc make a directory directory
%% @spec make_directory(bitstring()) -> {ok, nil} | {error, atom()}
make_directory(Path) ->
    case file:make_dir(Path) of
        ok ->
            {ok, nil};
        X ->
            X
    end.

%% @doc check if a path exists
%% @spec exists(binstring()) -> true | false
exists(Path) ->
    case file:read_file_info(Path) of
        {ok, _} ->
            true;
        _ ->
            false
    end.

%% @doc ensure all necessary directories for Path are created
%% @spec ensure_directory(bitstring()) -> {ok, nil} | {error, atom()}
ensure_directory(Path) ->
    case filelib:ensure_dir(Path) of
        ok ->
            {ok, nil};
        X ->
            X
    end.

%% @doc change the working directory
%% @spec cwd(bitstring()) -> {ok, nil} | {error, atom()}
cd(Path) ->
    Encoding = file:native_name_encoding(),
    NativePath = unicode:characters_to_binary(Path, Encoding, unicode),
    case file:set_cwd(NativePath) of
        ok ->
            {ok, nil};
        X ->
            X
    end.

%%
%% Private
%%
chardata_to_string(Data) ->
    case Data of
        X when is_binary(X) ->
            {ok, X};
        X when is_list(X) ->
            gleam_safe_characters_to_binary(X)
    end.

% TODO: this has more detail than just ok or error, there is also decode error and decode incomplete for utf16
gleam_safe_characters_to_binary(Chars) ->
    try unicode:characters_to_binary(Chars) of
        Result when is_binary(Result) ->
            {ok, Result};
        _ ->
            {error, nil}
    catch
        % {:error, encoded, rest} ->
        %   raise UnicodeConversionError, encoded: encoded, rest: rest, kind: :invalid
        % {:incomplete, encoded, rest} ->
        %   raise UnicodeConversionError, encoded: encoded, rest: rest, kind: :incomplete
        _ ->
            {error, nil}
    end.
